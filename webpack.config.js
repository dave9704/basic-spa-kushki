const webpackMerge = require("webpack-merge");
const singleSpaDefaults = require("webpack-config-single-spa-react-ts");
const path = require("path");

module.exports = (webpackConfigEnv) => {
  const defaultConfig = singleSpaDefaults({
    orgName: "kushki",
    projectName: "spa-backoffice-merchant-contact",
    webpackConfigEnv,
  });

  return webpackMerge.smart(defaultConfig, {
    externals: {
      kushki: "kushki",
    },
    output: {
      path: path.resolve(__dirname, "dist/merchant-contact"), // base path where to send compiled assets
    },
    module: {
      rules: [
        {
          test: /\.(png|jpe?g|gif|svg)$/i,
          use: [
            {
              loader: "file-loader",
            },
          ],
        },
      ],
    },
  });
};
