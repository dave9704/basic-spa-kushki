module.exports = {
  rootDir: "src",
  transform: {
    "^.+\\.(j|t)sx?$": "babel-jest",
  },
  moduleNameMapper: {
    "\\.(css)$": "identity-obj-proxy",
    "\\.(png)$": "identity-obj-proxy",
    "\\.(svg)$": "identity-obj-proxy",
  },
  setupFilesAfterEnv: [
    "../node_modules/@testing-library/jest-dom/dist/index.js",
    "../jest.setup.js",
  ],
  setupFiles: ["../jest.global.jsx"],
  coverageThreshold: {
    global: {
      branches: 100,
      functions: 100,
      lines: 100,
      statements: 100,
    },
  },
  coverageDirectory: "../coverage",
  collectCoverageFrom: ["./**/*.{ts,tsx}"],
  coveragePathIgnorePatterns: [
    "/node_modules/",
    "src/theme.ts",
    "src/set-public-path.tsx",
    "src/root.component.tsx",
    "navigation.component.tsx",
    "src/environments",
    "src/shared",
    "src/infrastructure",
    "src/store",
  ],
};
