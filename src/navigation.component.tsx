/* istanbul ignore file */
import React, { Suspense } from "react";
import { Dispatch } from "redux";
import { ThunkDispatch } from "redux-thunk";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { connect } from "react-redux";
import { IAppState } from "./store/reducer";
import { IAppAction, setNotification } from "./store/actionCreators";
import { SnackBarAlert } from "./components/common/SnackBar/SnackBarAlert";
import { INotification } from "./shared/infrastructure/interfaces/INotification";

// @ts-ignore
import Normalize from "react-normalize";
import { routes } from "./shared/infrastructure/routes";
import { MerchantContactIndex } from "./containers/MerchantContactIndex/MerchantContactIndex";

interface INavigationFunctionsProps {
  setNotification(payload: INotification): void;
}

interface INavigationStateProps {
  notification: INotification;
}

type TNavigationProps = INavigationFunctionsProps & INavigationStateProps;
const Navigation: React.FC<TNavigationProps> = (props: TNavigationProps) => {
  const handleCloseSnackbar = () => {
    props.setNotification({
      open: false,
      message: props.notification.message,
      type: props.notification.type,
    });
  };

  return (
    <>
      <Normalize />
      <BrowserRouter>
        <Suspense fallback={<div />}>
          <Switch>
            <Route
              path={`${routes.BASE_PATH_MERCHANT_CONTACT}${routes.INDEX}`}
              exact
              component={MerchantContactIndex}
            />
          </Switch>
        </Suspense>
        <SnackBarAlert
          type={props.notification.type}
          msg={props.notification.message}
          open={props.notification.open}
          handlerClose={handleCloseSnackbar}
        />
      </BrowserRouter>
    </>
  );
};

export const mapStateToProps: (state: IAppState) => INavigationStateProps = (
  state: IAppState
): INavigationStateProps => ({
  notification: state.notification!,
});

export const mapDispatchToProps: (
  dispatch: Dispatch
) => INavigationFunctionsProps = (
  dispatch: ThunkDispatch<IAppState, undefined, IAppAction>
): INavigationFunctionsProps => ({
  setNotification: (payload: INotification) =>
    dispatch(setNotification(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Navigation);
