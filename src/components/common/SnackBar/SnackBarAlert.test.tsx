import { configure, shallow, ShallowWrapper } from "enzyme";
import React from "react";
import Adapter from "enzyme-adapter-react-16";
import { SnackBarAlert, SnackbarAlertProps } from "./SnackBarAlert";
import { Snackbar } from "@material-ui/core";
import MuiAlert from "@material-ui/lab/Alert";

configure({ adapter: new Adapter() });

describe("SnackbarAlert component", () => {
  let props: SnackbarAlertProps;
  let wrapper: ShallowWrapper;

  beforeEach(() => {
    props = {
      msg: "tes",
      open: true,
      type: "success",
      handlerClose: jest.fn(),
    };

    wrapper = shallow(<SnackBarAlert {...props} />);
  });

  it("should render a snackbar alert", () => {
    expect(wrapper.find(Snackbar).length).toEqual(1);
    expect(wrapper.find(MuiAlert).length).toEqual(1);
  });
});
