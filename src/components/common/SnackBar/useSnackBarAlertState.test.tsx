import { configure, mount, ReactWrapper } from "enzyme";
import React from "react";
import Adapter from "enzyme-adapter-react-16";
import { SnackbarAlertProps } from "./SnackBarAlert";
import { useSnackBarAlertState } from "./useSnackBarAlertState";

configure({ adapter: new Adapter() });

describe("useSnackBarAlertState hook", () => {
  let props: SnackbarAlertProps;
  let wrapper: ReactWrapper;

  beforeEach(() => {
    props = {
      msg: "tes",
      open: true,
      type: "success",
      handlerClose: jest.fn(),
    };
    const SnackbarAlert = (propsTest: SnackbarAlertProps) => {
      const useProps = useSnackBarAlertState(propsTest);

      // @ts-ignore
      return <div {...useProps} />;
    };

    wrapper = mount(<SnackbarAlert {...props} />);
  });

  it("should have open prop", () => {
    expect(wrapper.childAt(0).prop("open")).toEqual(true);
  });
});
