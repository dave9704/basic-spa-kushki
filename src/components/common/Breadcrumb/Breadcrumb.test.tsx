import React from "react";
import { configure, shallow, ShallowWrapper } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import Breadcrumb, { BreadcrumProps } from "./Breadcrumb";

configure({ adapter: new Adapter() });

describe("Breadcrumb component", () => {
  let wrapper: ShallowWrapper;
  let props: BreadcrumProps;

  beforeEach(() => {
    props = {
      items: [{ label: "Inicio", url: "/dashboard" }],
      lastItem: "Smartlinks",
    };
    wrapper = shallow(<Breadcrumb {...props} />);
  });

  it("should be in the document", () => {
    wrapper.setProps({ property: "Testapp" });
    expect(wrapper.exists()).toEqual(true);
  });
});
