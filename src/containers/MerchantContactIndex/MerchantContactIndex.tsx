import React from "react";
import { Container, Typography } from "@material-ui/core";
import Breadcrumb from "../../components/common/Breadcrumb/Breadcrumb";
import { useMerchantContactIndexState } from "./state/useMerchantContactIndexState";

export const MerchantContactIndex: React.FC = () => {
  const { breadCrumbs } = useMerchantContactIndexState();
  return (
    <React.Fragment>
      <Container fixed>
        <Breadcrumb items={breadCrumbs.items} lastItem={breadCrumbs.lastItem} />
        <Typography variant="h1" color="primary">
          Gestión de contactos
        </Typography>
      </Container>
    </React.Fragment>
  );
};
