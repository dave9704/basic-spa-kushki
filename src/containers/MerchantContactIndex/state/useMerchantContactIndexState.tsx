import { useState } from "react";
import { BreadcrumProps } from "../../../components/common/Breadcrumb/Breadcrumb";
import { routes } from "../../../shared/infrastructure/routes";
import { useForm } from "react-hook-form";

export interface IMerchantContactIndexState {
  state: IState;
  handlers: {};
  breadCrumbs: BreadcrumProps;
}

export interface IState {}

export const useMerchantContactIndexState = (): IMerchantContactIndexState => {
  const [state] = useState<IState>({});
  const { register } = useForm({
    mode: "onBlur",
    reValidateMode: "onBlur",
  });
  console.log("for use form-hook", register);
  /*
   * You can use useSelector to access data from redux store
   * */
  /*
   * You can use useDispatch to execute functions of actionCreators
   * */
  return {
    state,
    handlers: {},
    breadCrumbs: {
      items: [
        {
          label: "Inicio",
          url: routes.DASHBOARD,
        },
      ],
      lastItem: "Gestión de contactos",
    },
  };
};
