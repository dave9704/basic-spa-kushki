import { configure, shallow, ShallowWrapper } from "enzyme";
import React from "react";
import Adapter from "enzyme-adapter-react-16";
import {
  IMerchantContactIndexState,
  useMerchantContactIndexState,
} from "./useMerchantContactIndexState";

configure({ adapter: new Adapter() });

jest.useFakeTimers();

jest.mock("react-router-dom", () => ({
  useHistory: () => ({
    push: jest.fn(),
  }),
}));

describe("useMerchantContactIndexState hook", () => {
  let wrapper: ShallowWrapper<IMerchantContactIndexState>;

  beforeEach(() => {
    mountWrapper();
  });

  function mountWrapper() {
    const MerchantContactIndex = () => {
      const useProps = useMerchantContactIndexState();

      // @ts-ignore
      return <div {...useProps} />;
    };

    wrapper = shallow<IMerchantContactIndexState>(<MerchantContactIndex />);
  }

  it("breadCrumbs - should have property items", () => {
    expect(wrapper.prop("breadCrumbs")).toBeDefined();
    expect(wrapper.prop("breadCrumbs")).toHaveProperty("items");
  });
});
