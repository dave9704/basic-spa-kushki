import React from "react";
import { configure, shallow, ShallowWrapper } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { Container, Typography } from "@material-ui/core";

import { MerchantContactIndex } from "./MerchantContactIndex";

configure({ adapter: new Adapter() });

jest.mock("./state/useMerchantContactIndexState", () => ({
  useMerchantContactIndexState: () => ({
    state: {},
    handlers: {},
    breadCrumbs: [],
  }),
}));

describe("MerchantContactIndex tests", () => {
  describe("MerchantContactIndex Container", () => {
    let wrapper: ShallowWrapper;

    beforeEach(() => {
      wrapper = shallow(<MerchantContactIndex />);
    });

    it("MerchantContactIndex test", () => {
      expect(wrapper.find(Container).length).toEqual(1);
      expect(Typography).toBeDefined();
    });
  });
});
