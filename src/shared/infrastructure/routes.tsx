export enum routes {
  MERCHANTS = "/merchant-list",
  BASE_PATH_MERCHANT_CONTACT = "/merchant-contact",
  INDEX = "/",
  DASHBOARD = "/dashboard",
}
