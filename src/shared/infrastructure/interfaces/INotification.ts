import { Color } from "@material-ui/lab/Alert";

export interface INotification {
  open: boolean;
  message: string;
  type: Color;
  metadata?: object;
}
