export interface Category {
  name: string;
  value: string | number;

  [k: string]: any;
}
