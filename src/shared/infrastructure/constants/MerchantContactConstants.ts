export interface ICountry {
  value: string;
  name: string;
  flag: string;
}
