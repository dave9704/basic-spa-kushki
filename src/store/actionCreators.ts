import { IAppState } from "./reducer";
import { ActionTypes } from "./actionTypes";
import { INotification } from "../shared/infrastructure/interfaces/INotification";

export type IAppAction = { type: string } & IAppState;

export const setNotification = (payload: INotification) => ({
  type: ActionTypes.SET_NOTIFICATION,
  notification: payload,
});
