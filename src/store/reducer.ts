import { IAppAction } from "./actionCreators";
import { INotification } from "../shared/infrastructure/interfaces/INotification";
import { ActionTypes } from "./actionTypes";

export interface IAppState {
  notification?: INotification;
}

export const INITIAL_STATE: IAppState = {
  notification: {
    type: "success",
    open: false,
    message: "",
  },
};

export const reducer = (
  state: IAppState = INITIAL_STATE,
  action: IAppAction
): IAppState => {
  switch (action.type) {
    case ActionTypes.SET_NOTIFICATION:
      return {
        ...state,
        notification: action.notification,
      };
    default:
      return state;
  }
};
